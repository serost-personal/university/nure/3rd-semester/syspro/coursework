import psutil
import platform
import subprocess
import json

def get_cpu_info():
    cpu_info = {}
    cpu_info['Brand'] = platform.processor()
    cpu_info['Physical cores'] = psutil.cpu_count(logical=False)
    cpu_info['Total cores'] = psutil.cpu_count(logical=True)
    cpu_info['Max Frequency'] = f"{psutil.cpu_freq().max:.2f}Mhz"
    cpu_info['Min Frequency'] = f"{psutil.cpu_freq().min:.2f}Mhz"
    cpu_info['Current Frequency'] = f"{psutil.cpu_freq().current:.2f}Mhz"
    cpu_info['Usage Per Core'] = [f"{x:.2f}%" for x in psutil.cpu_percent(percpu=True)]
    cpu_info['Total Usage'] = f"{psutil.cpu_percent()}%"
    return cpu_info

def get_ram_info():
    ram_info = {}
    svmem = psutil.virtual_memory()
    ram_info['Total'] = f"{svmem.total / (1024 ** 3):.2f} GB"
    ram_info['Available'] = f"{svmem.available / (1024 ** 3):.2f} GB"
    ram_info['Used'] = f"{svmem.used / (1024 ** 3):.2f} GB"
    ram_info['Percentage'] = f"{svmem.percent}%"
    return ram_info

def get_disk_info():
    disk_info = {}
    partitions = psutil.disk_partitions()
    for partition in partitions:
        partition_usage = psutil.disk_usage(partition.mountpoint)
        disk_info[partition.device] = {
            'Mountpoint': partition.mountpoint,
            'File system type': partition.fstype,
            'Total Size': f"{partition_usage.total / (1024 ** 3):.2f} GB",
            'Used': f"{partition_usage.used / (1024 ** 3):.2f} GB",
            'Free': f"{partition_usage.free / (1024 ** 3):.2f} GB",
            'Percentage': f"{partition_usage.percent}%"
        }
    return disk_info

def get_network_info():
    network_info = {}
    interfaces = psutil.net_if_addrs()
    for interface_name, interface_addresses in interfaces.items():
        addresses = []
        for address in interface_addresses:
            addresses.append({
                'family': address.family.name,
                'address': address.address,
                'netmask': address.netmask,
                'broadcast': address.broadcast
            })
        network_info[interface_name] = addresses
    return network_info

def get_gpu_info():
    try:
        output = subprocess.check_output(["lshw", "-json", "-C", "display"], universal_newlines=True, stderr=subprocess.PIPE)
        data = json.loads(output)
        gpus = []
        for item in data:
            if item.get('class') == 'display':
                gpus.append(item.get('product', 'Unknown GPU'))
        return gpus
    except Exception as e:
        print(f"Error retrieving GPU info: {e}")
        return []

def get_monitor_info():
    try:
        output = subprocess.check_output(["xrandr"], universal_newlines=True)
        monitors = []
        lines = output.splitlines()
        current_monitor = None
        for line in lines:
            if ' connected' in line:
                if current_monitor:
                    monitors.append(current_monitor)
                monitor_name = line.split()[0]
                resolutions = []
                current_monitor = {'name': monitor_name, 'resolutions': resolutions}
            elif current_monitor:
                if ' connected' not in line:
                    resolution = line.strip().split()[0]
                    resolutions.append(resolution)
        if current_monitor:
            monitors.append(current_monitor)
        return monitors
    except Exception as e:
        print(f"Error retrieving monitor info: {e}")
        return []

import tkinter as tk
from tkinter import messagebox, scrolledtext
from functools import partial
import yaml

def get_system_info():
    info = ""
    if cpu_var.get():
        info += "CPU Info:\n" + yaml.dump(get_cpu_info()) + "\n\n"
    if ram_var.get():
        info += "RAM Info:\n" + yaml.dump(get_ram_info()) + "\n\n"
    if disk_var.get():
        info += "Disk Info:\n" + yaml.dump(get_disk_info()) + "\n\n"
    if network_var.get():
        info += "Network Info:\n" + yaml.dump(get_network_info()) + "\n\n"
    if gpu_var.get():
        info += "GPU Info:\n" + yaml.dump(get_gpu_info()) + "\n\n"
    if monitor_var.get():
        info += "Monitor Info:\n" + yaml.dump(get_monitor_info()) + "\n\n"
    return info

def save_to_file():
    info = get_system_info()
    if info:
        try:
            with open("system_info.txt", "w") as file:
                file.write(info)
            messagebox.showinfo("Success", "Information saved to system_info.txt")
        except Exception as e:
            messagebox.showerror("Error", f"An error occurred: {str(e)}")
    else:
        messagebox.showwarning("No Information", "Please select at least one category to save.")

# GUI setup
root = tk.Tk()
root.title("System Information")

cpu_var = tk.BooleanVar()
ram_var = tk.BooleanVar()
disk_var = tk.BooleanVar()
network_var = tk.BooleanVar()
gpu_var = tk.BooleanVar()
monitor_var = tk.BooleanVar()

cpu_checkbtn = tk.Checkbutton(root, text="CPU", variable=cpu_var)
ram_checkbtn = tk.Checkbutton(root, text="RAM", variable=ram_var)
disk_checkbtn = tk.Checkbutton(root, text="Disk", variable=disk_var)
network_checkbtn = tk.Checkbutton(root, text="Network", variable=network_var)
gpu_checkbtn = tk.Checkbutton(root, text="GPU", variable=gpu_var)
monitor_checkbtn = tk.Checkbutton(root, text="Monitor", variable=monitor_var)

cpu_checkbtn.pack()
ram_checkbtn.pack()
disk_checkbtn.pack()
network_checkbtn.pack()
gpu_checkbtn.pack()
monitor_checkbtn.pack()

info_text = scrolledtext.ScrolledText(root, wrap=tk.WORD, width=40, height=10)
info_text.pack()

def display_info():
    info_text.delete(1.0, tk.END)
    info_text.insert(tk.END, get_system_info())

info_button = tk.Button(root, text="Get Information", command=display_info)
info_button.pack()

save_button = tk.Button(root, text="Save Information", command=save_to_file)
save_button.pack()

root.mainloop()